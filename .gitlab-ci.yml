stages:
  - test
  - integration
  - build

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_TLS_VERIFY: 1
  DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"

lint:
  stage: test
  image: pipelinecomponents/flake8
  tags:
    - thorchain
  script:
    - flake8 --exclude ./thornode_proto

unit-tests:
  stage: test
  image: docker/compose:alpine-1.29.2
  tags:
    - thorchain
  services:
    - docker:19-dind
  before_script:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27384#note_497228752
    - |
      for i in $(seq 1 30)
      do
          docker info && break
          echo "Waiting for docker to start"
          sleep 1s
      done
    - apk -U add git make
    - make build
  script:
    - RUNE=THOR.RUNE make test-coverage
    - RUNE=THOR.RUNE make test-coverage-report

smoke:
  stage: integration
  image: docker/compose:alpine-1.29.2
  tags:
    - thorchain
  services:
    - docker:19.03.13-dind
  artifacts:
    when: on_failure
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    paths:
      - ./logs/
  variables:
    HEIMDALL_REPO: https://gitlab.com/thorchain/heimdall.git
    HEIMDALL_IMAGE: registry.gitlab.com/thorchain/heimdall:develop
    MIDGARD_REPO: https://gitlab.com/thorchain/midgard.git
    MIDGARD_IMAGE: registry.gitlab.com/thorchain/midgard:develop
    THORNODE_REPO: https://gitlab.com/thorchain/thornode.git
    THORNODE_IMAGE: registry.gitlab.com/thorchain/thornode:develop
    THOR_BLOCK_TIME: 0.8s
    BLOCK_TIME: 0.8s
    BLOCK_SCANNER_BACKOFF: 0.8s
    ETH_BLOCK_TIME: "0"
  before_script:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27384#note_497228752
    - |
      for i in $(seq 1 30)
      do
          docker info && break
          echo "Waiting for docker to start"
          sleep 1s
      done
    - apk -U add git make protoc
    - docker pull $HEIMDALL_IMAGE || true
    - docker build --cache-from $HEIMDALL_IMAGE -t $HEIMDALL_IMAGE .
    - (git clone --single-branch -b $CI_COMMIT_REF_NAME $MIDGARD_REPO && cd ./midgard && IMAGE_NAME=$MIDGARD_IMAGE make build && cd ..) || (git clone --single-branch -b develop $MIDGARD_REPO && docker pull $MIDGARD_IMAGE)
    - git clone --single-branch -b $CI_COMMIT_REF_NAME $THORNODE_REPO || (git clone --single-branch -b develop $THORNODE_REPO && docker pull $THORNODE_IMAGE && docker tag $THORNODE_IMAGE registry.gitlab.com/thorchain/thornode:mocknet)
    - cd ./thornode && docker-compose -f build/docker/docker-compose.yml --profile mocknet --profile midgard up -d && cd ..
  script:
    - IMAGE_NAME=$HEIMDALL_IMAGE make smoke
  after_script:
    - ./thornode/scripts/docker_logs.sh

build:
  stage: build
  image: docker:stable
  only:
    - master
    - develop
  services:
    - docker:19.03.13-dind
  before_script:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27384#note_497228752
    - |
      for i in $(seq 1 30)
      do
          docker info && break
          echo "Waiting for docker to start"
          sleep 1s
      done
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA --tag $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    - docker push $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
